# Definiciones


### SavepointCase

Similar a SingleTransactionCase en que todos los métodos de prueba se ejecutan en una sola transacción, pero cada caso de prueba se ejecuta dentro de un punto de recuperación revertido (subtransacción).

Útil para casos de prueba que contienen pruebas rápidas pero con una configuración de base de datos significativa común a todos los casos (datos de prueba complejos en db): `setUpClass()` se puede usar para generar datos de prueba db una vez, luego todos los casos de prueba usan los mismos datos sin influenciarse entre sí pero sin tener que volver a crear los datos de prueba tampoco.


### `setUp`

El método `setUp()` es donde preparamos los datos y las variables que se van a utilizar. Normalmente los almacenaremos como atributos de clase, de modo que estén disponibles para ser utilizados en los métodos de prueba.

Las pruebas pueden ser numerosas y su configuración puede ser repetitiva. Afortunadamente, podemos factorizar el código de configuración implementando un método llamado `setUp()`, que el marco de prueba llamará automáticamente para cada prueba que ejecutamos:

```python 
class TestAlgo(TransactionCase):
	def setUp(self):
        super(TestAlgo, self).setUp()

        #Todas las declaraciones que se puedan utilizar mas adelante...
        #EX: 
        self.algo = self.env['modelo.algo']
        self.new_algo = self.algo.create({ 'name': 'algo'})

```

### Tests

primeros pasos: 
1. para que el odoo entiendan que son pruebas, lo primordial es que cada metodo tenga el prefijo `test_`
2. Declaramos la clases principal
3. Luego deifnimos los metodos a utilizar, como se establece en el paso uno precedida por el sufijo, mas el nombre del test `test_nombre_del_test`

```python 
class TestAlgo(TransactionCase):
	def setUp(self):
        super(TestAlgo, self).setUp()

        #...

    def test_algo_create(self):
    	algo = self.assertEqual(self.new_algo.name, 'algo')
    	print(algo)

```




### Botones: 

La acción del botón Toggle Done será muy simple: solo cambia la bandera `Active?`. Para la lógica de los registros, utiliza el decorador `@api.multi`. Aquí, self representará un conjunto de registros, y entonces deberíamos hacer un bucle a través de cada registro.

Dentro de la clase LibraryBooks, añade esto:

```python
@api.multi 
def do_toggle_done(self): 
    for books in self: 
        books.is_done = not books.active
    return True
```

El código pasa por todos los registros de tarea y, para cada uno, modifica el campo is_done, invirtiendo su valor. El método no necesita devolver nada, pero debemos tenerlo al menos para devolver un valor `True`. La razón es que los clientes pueden utilizar `XML-RPC` para llamar a estos métodos y este protocolo no admite funciones de servidor devolviendo sólo un valor `None`.

Para el botón Clear All Done, queremos ir un poco más lejos. Debe buscar todos los registros activos que están hechos, y hacerlos inactivos. Normalmente, se espera que los botones de formulario actúen sólo en el registro seleccionado, pero en este caso, queremos que actúe también en registros distintos del actual:

```python
@api.model 
def do_clear_done(self): 
    dones = self.search([('active', '=', True)]) 
    dones.write({'active': False}) 
    return True
```

En los métodos decorados con `@api.model`, la variable `self` representa el modelo sin registro en particular. Construiremos un conjunto de registros `dones` que contenga todas las tareas marcadas como terminadas. A continuación, establecemos el indicador `active` para `False` en ellos.

El método `write` establece los valores de una vez en todos los elementos del conjunto de registros. Los valores a escribir se describen utilizando un diccionario. Usar `write` àqui es más eficiente que iterar a través del conjunto de registros para asignar el valor a cada uno de ellos uno por uno.