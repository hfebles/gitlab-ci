# -*- coding: utf-8 -*-

from odoo.tests.common import TransactionCase, SavepointCase, Form

class TestAutho(TransactionCase):

    def test_some_action2(self):
        books = self.env['library.books']
        author = self.env['library.author']
        category = self.env['library.category']

        #create a new category
        new_category = category.create({ 'name': 'sciences', 'date_register': '1990-04-18'})

        #create a new author
        new_author = author.create({ 'author_name': 'William Bateson', 'date_register': '2019-11-06'})

        #create a new book
        books1 = books.create({
            'name': 'Mendels Principles of Heredity',
            'date_register': '1990-04-18',
            'author_name': new_author.id,
            'category': new_category.id
            })

        f = Form(books)
        f.name = 'Geometria'
        so = f.save()

        f1 = Form(author)
        f1.author_name = 'Pitagoras'
        so1 = f1.save()

        with Form(so1) as f2:
            so.author_name = f2.id


        print('\n\t\t\t====================================================')
        print('\n\t\t\t========== [ Test  realizado con exito ] ==========')
        print('\n\t\t\t====================================================')