# -*- coding: utf-8 -*-


from datetime import date
from odoo.exceptions import UserError
from odoo.tests.common import TransactionCase, SavepointCase, Form
from odoo import fields

class TestAll(SavepointCase):

    def setUp(self):
        super(TestAll, self).setUp()

        self.books = self.env['library.books']
        self.author = self.env['library.author']
        self.category = self.env['library.category']

        # Create a new category
        self.new_category = self.category.create({ 'name': 'sciences', 'date_register': '1990-04-18'})

        # Create a new author
        self.new_author = self.author.create({ 'author_name': 'William Bateson', 'date_register': '2019-11-06'})


        t0 = date.today()

        # Create a news books
        self.books1 = self.books.create({
            'name': 'Mendels Principles of Heredity',
            'date_register': fields.Date.to_string(t0),
            'author_name': self.new_author.id,
            'category': self.new_category.id,
            'active': False,
            'is_done': True
            })

        self.books2 = self.books.create({
            'name': 'Mendels Principles of Heredity',
            'date_register': '1990-04-18',
            'author_name': self.new_author.id,
            'category': self.new_category.id,
            'active': True,
            'is_done': False
            })

        self.books3 = self.books.create({
            'name': 'Mendels Principles of Heredity',
            'date_register': '2010-06-24',
            'author_name': self.new_author.id,
            'category': self.new_category.id
            })

        self.books4 = self.books.create({
            'name': 'Mendels Principles of Heredity',
            'date_register': '1887-05-07',
            'author_name': self.new_author.id,
            'category': self.new_category.id,
            'is_done': True
            })

        
    def test_one_create_relationship(self):

        t0 = date.today()

        try:
            self.assertEqual(self.books1.name, 'Mendels Principles of Heredity')
            self.assertEqual(self.books2.author_name.id, self.new_author.id)
            self.assertEqual(self.books3.category.id, self.new_category.id)
            self.assertEqual(self.books1.date_register, t0)
            self.assertTrue(self.books3.active)
            
        except AssertionError:
            print('\n\t\t\t=================================================================================')
            print('\n\t\t\t========== [ test_one_create_relationship no fue realizado con exito ] ==========')
            print('\n\t\t\t=================================================================================')
            print('\n\t\t\t\t\t========== [ ERRORES ] ==========\n')
            raise
        else:
            print('\n\t\t\t==========================================================================')
            print('\n\t\t\t========== [ test_one_create_relationship realizado con exito ] ==========')
            print('\n\t\t\t=========================================================================\n')

    def test_buttons(self):
        
        try:
            self.books1.do_toggle_done() 
            self.assertTrue(self.books1.is_done)

            self.books.do_clear_done() 
            self.assertFalse(self.books4.active)
            
        except AssertionError:
            print('\n\t\t\t=================================================================')
            print('\n\t\t\t========== [ test_buttons no fue realizado con exito ] ==========')
            print('\n\t\t\t=================================================================')
            print('\n\t\t\t\t\t========== [ ERRORES ] ==========\n')
            raise
        else:
            print('\n\t\t\t==========================================================')
            print('\n\t\t\t========== [ test_buttons realizado con exito ] ==========')
            print('\n\t\t\t========================================================\n')


    def test_form(self):

        try:

            f = Form(self.books)
            f.name = 'Geometria'
            so = f.save()

            f1 = Form(self.author)
            f1.author_name = 'Pitagoras'
            so1 = f1.save()

            with Form(so1) as f2:
                so.author_name = f2.id

        except AssertionError:
            print('\n\t\t\t=================================================================')
            print('\n\t\t\t========== [ test_form no fue realizado con exito ] ==========')
            print('\n\t\t\t=================================================================')
            print('\n\t\t\t\t\t========== [ ERRORES ] ==========\n')
            raise
        else:
            print('\n\t\t\t==========================================================')
            print('\n\t\t\t========== [ test_form realizado con exito ] ==========')
            print('\n\t\t\t========================================================\n')
