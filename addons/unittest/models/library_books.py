# -*- coding: utf-8 -*- 

from odoo import models, fields, api

class LibraryBooks(models.Model): 
    _name = 'library.books' 
    _description = 'Library Books.'

    name = fields.Char(required=True) 
    author_name = fields.Many2one('library.author', 'Author Name')
    date_register = fields.Date()
    active = fields.Boolean('Active?', default=True)
    category = fields.Many2one('library.category', 'Category Name')
    is_done = fields.Boolean('Done?') 

    @api.multi 
    def do_toggle_done(self):
        for books in self: 
            books.active = not books.active 
        return True

    @api.model 
    def do_clear_done(self): 
        dones = self.search([('is_done', '=', True)]) 
        dones.write({'active': False}) 
        return True



