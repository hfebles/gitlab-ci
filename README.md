# Pruebas unitarias para odoo


## Definiciones

### `SavepointCase`
Todos los métodos de prueba se ejecutan en una sola transacción, pero cada caso de prueba se ejecuta dentro de un punto de recuperación revertido (subtransacción).

Útil para casos de prueba que contienen pruebas rápidas pero con una configuración de base de datos significativa común a todos los casos (datos de prueba complejos en db): `setUpClass()` se puede usar para generar datos de prueba db una vez, luego todos los casos de prueba usan los mismos datos sin influenciarse entre sí pero sin tener que volver a crear los datos de prueba tampoco.

### `setUp`
El método `setUp()` es donde preparamos los datos y las variables que se van a utilizar. Normalmente los almacenaremos como atributos de clase, de modo que estén disponibles para ser utilizados en los métodos de prueba.

Las pruebas pueden ser numerosas y su configuración puede ser repetitiva. Afortunadamente, podemos factorizar el código de configuración implementando un método llamado `setUp()`, que el marco de prueba llamará automáticamente para cada prueba que ejecutamos:

```python 
class TestAlgo(TransactionCase):
	def setUp(self):
        super(TestAlgo, self).setUp()

        #Todas las declaraciones que se puedan utilizar mas adelante...
        #EX: 
        self.algo = self.env['modelo.algo']
        self.new_algo = self.algo.create({ 'name': 'algo'})

```
### Tests
primeros pasos: 
1. para que el odoo entiendan que son pruebas, lo primordial es que cada metodo tenga el prefijo `test_`
2. Declaramos la clases principal
3. Luego deifnimos los metodos a utilizar, como se establece en el paso uno precedida por el sufijo, mas el nombre del test `test_nombre_del_test`

```python 
class TestAlgo(TransactionCase):
	def setUp(self):
        super(TestAlgo, self).setUp()

        #...

    def test_algo_create(self):
    	algo = self.assertEqual(self.new_algo.name, 'algo')
    	print(algo)

```
### Botones: 

La acción del botón Toggle Done será muy simple: solo cambia la bandera `Active?`. Para la lógica de los registros, utiliza el decorador `@api.multi`. Aquí, self representará un conjunto de registros, y entonces deberíamos hacer un bucle a través de cada registro.

Dentro de la clase `LibraryBooks`, añade esto:

```python
@api.multi 
def do_toggle_done(self): 
    for books in self: 
        books.is_done = not books.active
    return True
```

El código pasa por todos los registros de tarea y, para cada uno, modifica el campo is_done, invirtiendo su valor. El método no necesita devolver nada, pero debemos tenerlo al menos para devolver un valor `True`. La razón es que los clientes pueden utilizar `XML-RPC` para llamar a estos métodos y este protocolo no admite funciones de servidor devolviendo sólo un valor `None`.

Para el botón Clear All Done, queremos ir un poco más lejos. Debe buscar todos los registros activos que están hechos, y hacerlos inactivos. Normalmente, se espera que los botones de formulario actúen sólo en el registro seleccionado, pero en este caso, queremos que actúe también en registros distintos del actual:

```python
@api.model 
def do_clear_done(self): 
    dones = self.search([('active', '=', True)]) 
    dones.write({'active': False}) 
    return True
```

En los métodos decorados con `@api.model`, la variable `self` representa el modelo sin registro en particular. Construiremos un conjunto de registros `dones` que contenga todas las tareas marcadas como terminadas. A continuación, establecemos el indicador `active` para `False` en ellos.

El método `write` establece los valores de una vez en todos los elementos del conjunto de registros. Los valores a escribir se describen utilizando un diccionario. Usar `write` àqui es más eficiente que iterar a través del conjunto de registros para asignar el valor a cada uno de ellos uno por uno.



### Resultado exitoso de las pruebas:

```bash 
========================================================
========== [ test_buttons realizado con exito ] ========
========================================================
=======================================================
========== [ test_form realizado con exito ] ==========
=======================================================
=========================================================================
========== [ test_one_create_relationship realizado con exito ] =========
=========================================================================

```


# Intregacion CI/CD gitlab.com

GitLab CI / CD sirve para numerosos propósitos, para construir, probar e implementar su aplicación desde GitLab a través de los métodos de Integración continua, Entrega continua e Implementación continua.

Para implementar GitLab CI / CD, lo primero que necesita es un archivo de configuración llamado `.gitlab-ci.yml` ubicado en el directorio raíz de su repositorio.

Lo que este archivo realmente hace es decirle al `GitLab Runner` que ejecute los scripts como lo haría desde la línea de comandos. El `Runner` actúa como tu terminal. GitLab CI / CD le dice al `Runner` qué comandos ejecutar. Ambos están integrados en GitLab, y no necesita configurar nada para que funcionen.

### Como construir el gitlab:
#### .gitlab-ci.yml

1. `image` lo primero que buscará `GitLab Runner` en su `.gitlab-ci.yml` es una imagen de Docker que especifique qué necesita en su contenedor para ejecutar ese script.\
Usa imágenes acoplables, `imagen:nombre` e `imagen:punto_de_entrada`.\
Para obtener mas informacion de las imagenes podemos visitar la siguiente pagina https://hub.docker.com.\
Ejemplos:

```yml
# Ubuntu 18
image: ubuntu:18.04
# Phyton
image: python:3.5.9-alpine3.10
# ontenedor dockers
image: docker:stable
```

2. Se utiliza para especificar una imagen Docker de servicio, vinculada a una base especificada en la imagen.\
Ejemplos de definiciones simples `.gitlab-ci.yml.`: `servicios:nombre-version`.\
Para obtener mas informacion de los servicios podemos visitar la siguiente pagina https://hub.docker.com.

```yml
services:
  - docker:stable-dind
```

3. **Opcional** Declaramos variables a utilizar durante el flujo de las pruebas.

```yml
variables:
# Nombre de la carpeta donde almacenamos los ficheros extraidos del repositorio
  DC_PROJECT: 'odoo_test_unittest'
# Variables reservadas del gitlab para registrar el contenedor de pruebas
  IMAGE_TAG: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
```

4. Otro concepto interesante a tener en cuenta son las etapas de construcción. Su aplicación puede pasar por muchas pruebas y otras tareas hasta que se implemente en entornos de preparación o producción. Hay tres etapas predeterminadas en GitLab CI: compilar, probar e implementar. Para especificar en qué etapa se está ejecutando su trabajo, simplemente agregue otra línea a su CI:

```yml
stages:
  - build
  - test
  - deploy
```

5. **Opcional** Para evitar ejecutar el mismo script varias veces en sus trabajos, puede agregar el parámetro `before_script`, en el que especifica qué comandos desea ejecutar para cada trabajo. En nuestro ejemplo, observe que ejecutamos la instalación de paquetes para trabajos, páginas y pruebas. No necesitamos repetirlo.

```yml
before_script:
  - docker login -u gitlab-ci-token -p $CI_JOB_TOKEN $CI_REGISTRY
  - apk update
  - apk upgrade
  - apk add py-pip
  - apk add py3-pip
  - pip install --upgrade pip
  - pip3 install docker-compose
  - echo $IMAGE_TAG
```

6. Desarrollo de los escenarios o `stages`, aqui se escriben las instrucciones que ejecutan las pruebas y todo aquello que desee ser probado.\
Las siguientes son explicaciones detalladas de los parámetros utilizados para configurar las canalizaciones de CI/CD.\
* __`script`__ es la única palabra clave requerida que necesita un trabajo. Es un script de shell ejecutado por el Runner.

* __`tags`__ Las etiquetas se utilizan para seleccionar corredores específicos de la lista de todos los corredores que pueden ejecutar este proyecto.

*
`script`
```yml
job:
  script:
    - uname -a
    - bundle exec rspec
```
`tags`
```yml
job:
  tags:
    - ruby
    - postgres
```
**Ejemplo:**

```yml
test:
  stage: test
  script:
    - docker-compose -p $DC_PROJECT run --rm odoo run_test.sh

  tags:
    - docker
```

### Dockerfile

1. Version de la aplicacion con la que trabajaremos y manteiner que lo realizo

```
FROM odoo:12.0
MAINTAINER Jesus Hernandez <jehfebles@gmail.com>
```

2. Usuario que crea y ejecuta el contenedor.
```
USER root
```
3. Preconfiguracion del contenedor a su vez instalamos el git para poder obtener los ficheros de nuestro proyecto.
```
RUN apt-get update && apt-get install -y git-core && \
    git config --global user.name "Odoo" && \
    git config --global user.email "root@localhost"

RUN pip3 install wheel
```

4. Extraemos los ficheros de nuestro repositorio e instalamos los requerimientos del odoo 12.0.
4.1 Copiamos los ficheros sh que contiene la ejecucion de las pruebas.
```
COPY ./requirements.txt .
RUN pip3 install -r requirements.txt

COPY ./addons /mnt/extra-addons/
COPY .git /mnt/.git/
COPY ./docker_files/run_test.sh /usr/local/bin/run_test.sh


ENV THIRD_PARTY_ADDONS /usr/lib/python3/dist-packages/odoo/addons
```
5. Configuramos los puertos y el usuario que ejecutara las pruebas dentro del contenedor.
```
EXPOSE 8069 8071
USER odoo
```


### docker-compose.yml 
Este archivo contiene la ejecucion de los servicios que se instalaran el contedor, con las instrucciones especificas de manera tal que se ejecute lo mas rapido posible.

```yml
version: '3'
services:
  odoo:
    build:
      context: ./
      dockerfile: Dockerfile
    volumes:
      - odoo-web-data:/var/lib/odoo
    ports:
      - "8069:8069"
    depends_on:
      - db
  db:
    image: postgres:9.6
    environment:
      - POSTGRES_PASSWORD=odoo
      - POSTGRES_USER=odoo
      - PGDATA=/var/lib/postgresql/data/pgdata
    volumes:
      - odoo-db-data:/var/lib/postgresql/data/pgdata
    expose:
      - 5432
volumes:
  odoo-web-data:
  odoo-db-data:
```


## Ficheros de apoyo

1. run_test.sh
2. run_test2.sh

### run_test.sh

Este fichero inicia el odoo, crea el archivo de configuracion y a su vez instala el modulo al que le realizaremos las pruebas, en una segunda ejecucion actualiza el modulo y corre el test

```bash
#!/usr/bin/env bash
odoo -c /etc/odoo/odoo.conf -s --db_host=db -r odoo -w odoo -d odoo -i unittest --stop-after-init && 
odoo -c /etc/odoo/odoo.conf -u unittest --stop-after-init --test-enable
```

### run_test2.sh

Este fichero instala el git, clonamos el repositorio y luego hacemos merge en caso de que las pruebas funcionaran con exito.
```bash
#!/usr/bin/env bash
apt install git
git clone https://gitlab.com/hfebles/gitlab-ci &&
cd gitlab-ci &&
git checkout test && 
git merge feature/jesus && 
git push
```
